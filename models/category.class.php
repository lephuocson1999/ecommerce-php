<?php 

    require_once("/database/db.class.php");

    class Category{
        public $cateID;
        public $categoryName;

        public function __construct($cate_name)
        {
            # code...
            $this->categoryName = $cate_name;
        }

        public static function list_category()
        {
            # code...
            $db  = new Db();
            $sql = "SELECT * FROM tbl_category";
            $result = $db->select_to_array($sql);
            return $result;
        }

        public static function list_product_of_category($categoryID)
        {
            # code...
            $db  = new Db();
            $sql = "SELECT * FROM tbl_product WHERE category_id = '$categoryID' ";
            $result = $db->select_to_array($sql);
            return $result;
        }
    }

?>