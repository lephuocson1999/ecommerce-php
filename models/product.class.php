
<?php

    require_once("/database/db.class.php");

    class Product{
        public $sanpham_id;
        public $sanpham_gia;
        public $category_id;
        public $sanpham_name;
        public $sanpham_chitiet;
        public $sanpham_mota;
        public $sanpham_image;

        public function __construct($pro_name, $cate_id, $price, $detail, $desc, $picture)
        {
            # code...
            $this->sanpham_name         = $pro_name;
            $this->category_id          = $cate_id;
            $this->sanpham_gia          = $price;
            $this->sanpham_chitiet      = $detail;
            $this->sanpham_mota         = $desc;
            $this->sanpham_image        = $picture;
        }

        public function save()
        {
            # code...

            $file_temp = $this->picture['tmp_name'];
            $user_file = $this->picture['name'];
            $timestamp = date("Y").date("m").date("d").date("h").date("i").date("s");
            $filepath  = "uploads/".$timestamp.$user_file;
            if( move_uploaded_file( $file_temp, $filepath ) == false ){
                return false;
            }

            $db = new Db();

            $sql = "INSERT INTO Product (ProductName, CateID, Price, Quantity, Description, Picture) 
                    VALUES ('$this->productName', 
                            '$this->cateID',
                            '$this->price',
                            '$this->quantity',
                            '$this->description',
                            '$filepath')";
        
            $result = $db->query_execute($sql);
            return $result;
        }

        public function list_product()
        {
            # code...
            $db = new Db();
            $sql = "SELECT * FROM product";
            $result = $db->select_to_array($sql);
            return $result;
        }

        public function list_product_by_cateid( $category_id )
        {
            # code...
            $db = new Db();
            $sql = "SELECT * FROM tblsanpham WHERE category_id='$category_id'";
            $result = $db->select_to_array($sql);
            return $result;
        }

        public function list_product_relate( $cateid, $id )
        {
            # code...
            $db = new Db();
            $sql = "SELECT * FROM product WHERE CateID='$cateid' AND productID!= '$id'";
            $result = $db->select_to_array($sql);
            return $result;
        }

        public static function get_product( $id )
        {
            # code...
            $db = new Db();
            $sql = "SELECT * FROM product WHERE productID='$id'";
            $result = $db->select_to_array($sql);
            return $result;
        }

        public static function remove_product( $id )
        {
            # code...
            $db = new Db();
            $sql = "DELETE FROM product WHERE productID='$id'";
            $result = $db->select_to_array($sql);
            return $result;
        }
    }
?>