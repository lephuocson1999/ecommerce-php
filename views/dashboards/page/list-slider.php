<?php
   include_once ('../partials/nav-bar.php');
   include_once ('../partials/side-bar.php');
   include_once ('../../../database/db.class.php');
   ?>
<div class="main-panel">
   <div class="content-wrapper">
      <div class="row">
         <div class="col-12 grid-margin">
            <div class="card">
               <div class="card-body">
                  <h4 class="card-title">Thêm banner</h4>
                  <form class="form-sample">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Tên banner</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" />
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-sm-3 col-form-label">Hình ảnh</label>
                              <div class="col-sm-9">
                                 <input type="file" class="form-control" />
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-sm-3 col-form-label"></label>
                              <div class="col-sm-9">
                                 <input class="btn btn-outline-dark btn-rounded btn-fw" type="submit" name="thembanner" value="Thêm banner">
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="col-lg-12 stretch-card">
            <div class="card">
               <div class="card-body">
                  <h4 class="card-title">Danh sách banner</h4>
                  <table class="table table-success">
                     <thead>
                        <tr>
                           <th> # </th>
                           <th> Name </th>
                           <th> Active </th>
                           <th> Image </th>
                           <th> # </th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $sql_slide = mysqli_query($con, "SELECT * FROM tbl_slider ORDER BY slider_id DESC");
                           while ($row_sanpham = mysqli_fetch_array($sql_slide))
                           {
                           ?>
                        <tr class="table-success">
                           <td> <?php echo $row_sanpham['slider_id'] ?> </td>
                           <td> <?php echo $row_sanpham['slider_caption'] ?> </td>
                           <td> 
                              <?php
                                 if ($row_sanpham['slider_active'] == 1)
                                 {
                                     echo "Active";
                                 }
                                 else
                                 {
                                     echo "Not Active";
                                 }
                                 ?>
                           </td>
                           <td> <img class="" src="../../../img/<?php echo $row_sanpham["slider_image"]; ?>" alt=""></td>
                           <td> 
                              <button type="button" class="btn btn-icons btn-rounded btn-outline-info">
                              <i class="mdi mdi-pencil"></i>
                              </button>
                              <button type="button" class="btn btn-icons btn-rounded btn-outline-warning">
                              <i class="mdi mdi-close"></i>
                              </button>
                           </td>
                        </tr>
                        <?php
                           }
                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
  include_once ('../partials/footer.php');
?>