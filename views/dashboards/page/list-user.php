<?php
    include_once('../partials/nav-bar.php');
    include_once('../partials/side-bar.php');
    include_once('../../../database/db.class.php');
?>
<div class="main-panel">
    <div class="content-wrapper">
    <div class="row">
<div class="col-lg-12 stretch-card">
    <div class="card">
        <div class="card-body">
        <h4 class="card-title">DANH SÁCH KHÁCH HÀNG</h4>
        <p class="card-description"> Add class <code>.table-{color}</code> </p>
        <table class="table table-success">
            <thead>
            <tr>
                <th> # </th>
                <th> Name </th>
                <th> Phone </th>
                <th> Address </th>
                <th> Email </th>
                <th> # </th>
            </tr>
            </thead>
            <tbody>
            <?php
                $sql_user  = mysqli_query($con,"SELECT * FROM tbl_khachhang ORDER BY khachhang_id DESC");
                while($row_sanpham = mysqli_fetch_array($sql_user)){ 
            ?>
                <tr class="table-success">
                    <td> <?php echo $row_sanpham['khachhang_id'] ?> </td>
                    <td> <?php echo $row_sanpham['name'] ?> </td>
                    <td> <?php echo $row_sanpham['phone'] ?> </td>
                    <td> <?php echo $row_sanpham['address'] ?> </td>
                    <td> <?php echo $row_sanpham['email'] ?> </td>
                    <td> 
                        <button type="button" class="btn btn-icons btn-rounded btn-outline-info">
                            <i class="mdi mdi-pencil"></i>
                        </button>
                        <button type="button" class="btn btn-icons btn-rounded btn-outline-warning">
                            <i class="mdi mdi-close"></i>
                          </button>
                    </td>
                </tr>
            <?php
                } 
            ?>
            <!-- <tr class="table-warning">
                <td> 2 </td>
                <td> Messsy Adam </td>
                <td> Flash </td>
                <td> $245.30 </td>
                <td> July 1, 2015 </td>
            </tr>
            <tr class="table-danger">
                <td> 3 </td>
                <td> John Richards </td>
                <td> Premeire </td>
                <td> $138.00 </td>
                <td> Apr 12, 2015 </td>
            </tr>
            <tr class="table-success">
                <td> 4 </td>
                <td> Peter Meggik </td>
                <td> After effects </td>
                <td> $ 77.99 </td>
                <td> May 15, 2015 </td>
            </tr>
            <tr class="table-primary">
                <td> 5 </td>
                <td> Edward </td>
                <td> Illustrator </td>
                <td> $ 160.25 </td>
                <td> May 03, 2015 </td>
            </tr> -->
            </tbody>
        </table>
        </div>
    </div>
</div>
</div>
    </div>
</div>
<?php
    include_once('../partials/footer.php');
?>